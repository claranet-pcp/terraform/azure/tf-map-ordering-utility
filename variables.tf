variable "input_map" {
  description = "Mandatory. Reference to the map object you wish to obtain sorted keys for."
  // Type omitted for dynamic parsing
}

variable "output_indexes" {
  description = "Mandatory. Map of lists to provide alternate index orders for output."
  // Type omitted for dynamic parsing
}