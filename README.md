# Terraform Map Ordering Utility

Terraform cannot honour the ordering of a map, as it sorts the keys lexicographically. This causes issues with certain resources from some providers (e.g. `azurerm.azurerm_application_gateway`), when changes are made via the Azure Portal the API returns object properties in a _different order_ (the last modified item having its list index changed, skewing that list of properties).

If using a `dynamic` block with `for_each` based on a map, the in-memory state refresh detects this re-ordering as drift and attempts to re-apply properties in the new order. This behaviour can be undesirable and cause infrastructure issues.

## What this module does

This module provides the ability to take a `map(map(any))`, and an arbitrary set of lists to provide a manual key indexing strategy. If a single `dynamic` block falls out of sync because it was changed manually in the Portal, you simply add another `list` containing the desired sort order _for that one named output_. In the resource you then change the reference from the existing `var`/`local` to `module.your_moduleref.outputs["<your desired sort index name>"]`.

Because lists retain their initial sort order, this is achieved for map keys under the hood by dynamically calculating a two character prefix for each key, based on the desired ordering inputs. When terraform sorts these lexicographically, they are still in the desired order.

## Quick Start

You can use the following Terraform snippet as a boilerplate, replace the variables with your own values to get you started.

> Please note: You must remember to slice the first three characters from `each.key` to remove the ordering prefix before assigning the key to a property value.

```hcl
module "my_map_reordering" {
  source = "git::https://gitlab.com/claranet-pcp/terraform/azure/tf-map-ordering-utility.git?ref=v1.0.0"
  
  // This is the map you want to order the keys on
  input_map = {
    myobj1 = {
      key = value
      ...
    }
    myobj2 = {
      key = value
      ...
    }
    myobj3 = {
      key = value
      ...
    }
  }

  // This is the list of ordered indexes you would like
  output_indexes = {
    "order_a" = ["myobj3", "myobj1", "myobj2"]
    "order_b" = ["myobj2", "myobj3", "myobj1"]
  }
}
```

In the above example, the supplied map would have the following outputs;

```hcl
// This will provide a custom prefix ordering for the "order_a" list
module.my_map_reordering.outputs.custom["order_a"] #=> 3, 1, 2

// This will provide a custom prefix ordering for the "order_b" list
module.my_map_reordering.outputs.custom["order_b"] #=> 2, 3, 1

// This is the default map ordering output
module.my_map_reordering.outputs.default #=> 1, 2, 3
```

## Variables

| Name | Description |
|------|-------------|
| `input_map` | Mandatory. Reference to the map object you wish to obtain sorted keys for. |
| `output_indexes` | Mandatory. Map of lists to provide alternate index orders for output. |