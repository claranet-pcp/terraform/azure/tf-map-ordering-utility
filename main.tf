locals {
  // The prefix lookup table allows us to easily convert count.index into an
  // indexed char. Because lists honour the initial supplied index, we use 
  // this as a lookup table to dynamically generate two-character uppercase 
  // prefixes for output map keys. When these are automatically returned by 
  // terraform in lexicographical order, the desired order is preserved.
  prefix_lookup = {
    "0"  = "A"
    "1"  = "B"
    "2"  = "C"
    "3"  = "D"
    "4"  = "E"
    "5"  = "F"
    "6"  = "G"
    "7"  = "H"
    "8"  = "I"
    "9" = "J"
    "10" = "K"
    "11" = "L"
    "12" = "M"
    "13" = "N"
    "14" = "O"
    "15" = "P"
    "16" = "Q"
    "17" = "R"
    "18" = "S"
    "19" = "T"
    "20" = "U"
    "21" = "V"
    "22" = "W"
    "23" = "X"
    "24" = "Y"
    "25" = "Z"
  }

  // Create a for_each'able map for each supplied index list, this allows
  // ongoing list management; even against resources which may require dynamic
  // blocks in a different order per block.
  custom = { for item in keys(var.output_indexes) : item => {
    for list_index, index_name in var.output_indexes[item] : 
      "${lookup(local.prefix_lookup, tostring(floor((list_index/26))), -1)}${lookup(local.prefix_lookup, tostring(list_index), -1)}-${index_name}" => lookup(var.input_map, index_name)
    }
  }
}

// Default return with lexicographically ordered keys
output "default" {
  value = var.input_map
}

// Return all the custom key ordering
output "custom" {
  value = local.custom
}
