terraform {
  required_version = "~> 0.14.4"
}

// Define our test data in a locals block
locals {
  // The map we want to order the keys on
  input_map = {
    engage_psyon_co_uk = {
      url  = "engage.psyon.co.uk"
      cert = "engage-psyon-co-uk-2019"
      name = "engage-psyon-co-uk"
    }
    my_lcpcloudbenefits_com = {
      url  = "my.lcpcloudbenefits.com"
      cert = "my-lcpcloudbenefits-com-2020"
      name = "my-lcpcloudbenefits-com"
    }
    my_zestbenefits_com = {
      url  = "my.zestbenefits.com"
      cert = "my-zestbenefits-com-2020"
      name = "my-zestbenefits-com"
    }
    mybenefits_benni_co_uk = {
      url  = "mybenefits.benni.co.uk"
      cert = "mybenefits-benni-co-uk-2020"
      name = "mybenefits-benni-co-uk"
    }
    worklifebenefits_uk = {
      url  = "worklifebenefits.uk"
      cert = "worklifebenefits-uk-2020"
      name = "worklifebenefits-uk"
    }
    www_edenred_flex_co_uk = {
      url  = "www.edenred-flex.co.uk"
      cert = "www-edenred-flex-co-uk-2019"
      name = "www-edenred-flex-co-uk"
    }
    www_honanbenefits_com = {
      url  = "www.honanbenefits.com"
      cert = "www-honanbenefits-com-2020"
      name = "www-honanbenefits-com"
    }
    www_myworkbenefits_co_uk = {
      url  = "www.myworkbenefits.co.uk"
      cert = "www-myworkbenefits-co-uk-2020"
      name = "www-myworkbenefits-co-uk"
    }
    www_nwgtapinto_co_uk = {
      url  = "www.nwgtapinto.co.uk"
      cert = "www-nwgtapinto-co-uk-2018"
      name = "www-nwgtapinto-co-uk"
    }
    www_qchoice_co_uk = {
      url  = "www.qchoice.co.uk"
      cert = "www-qchoice-co-uk-2020"
      name = "www-qchoice-co-uk"
    }
    www_worklife_co_uk = {
      url  = "www.worklife.co.uk"
      cert = "www-worklife-co-uk-2020"
      name = "www-worklife-co-uk"
    }
    aeconnect2_adp_com = {
      url  = "aeconnect2.adp.com"
      cert = "aeconnect2-adp-com-2020"
      name = "aeconnect2-adp-com"
    }
    www_bartlettbenefits_com = {
      url  = "www.bartlettbenefits.com"
      cert = "www-bartlettbenefits-com-2020"
      name = "www-bartlettbenefits-com"
    }
  }
  // List of maps detailing how you would like the keys ordered
  output_indexes = {
    my_first_order = [
      "engage_psyon_co_uk",
      "my_lcpcloudbenefits_com",
      "my_zestbenefits_com",
      "mybenefits_benni_co_uk",
      "worklifebenefits_uk",
      "www_edenred_flex_co_uk",
      "www_honanbenefits_com",
      "www_nwgtapinto_co_uk",
      "www_qchoice_co_uk",
      "www_worklife_co_uk",
      "aeconnect2_adp_com",
      "www_bartlettbenefits_com"
    ]
    my_second_order = [
      "www_bartlettbenefits_com",
      "www_nwgtapinto_co_uk",
      "my_lcpcloudbenefits_com",
      "my_zestbenefits_com",
      "engage_psyon_co_uk",
      "aeconnect2_adp_com",
      "www_worklife_co_uk",
      "www_qchoice_co_uk",
      "mybenefits_benni_co_uk",
      "www_edenred_flex_co_uk",
      "worklifebenefits_uk",
      "www_honanbenefits_com"
    ]
  }
}

// Import our current module branch
module "list_ordering_test" {
  source         = "../"
  input_map      = local.input_map
  output_indexes = local.output_indexes
}

// Test the outputs
output "list_ordering_test_default" {
  value = module.list_ordering_test.default
}

output "list_ordering_test_custom" {
  value = module.list_ordering_test.custom
}